<h1>Blog Articles</h1>
<p><?= $this->Html->link("Add Article",["action" => "add"])?></p>
<p><?= $this->Html->link("Logout",["controller"=>"users","action"=>"logout"])?></p>
<?= $this->Form->create(null, ['type' => 'get', 'class' => 'form-inline']) ?>
        <?= $this->Form->text('search', ['class' => 'form-control', 'label' => false]) ?>
        <?= $this->Form->button(__('Search'), ['class' => 'btn btn-default']) ?>
        <?= $this->Form->end(); ?>
<table>
	<tr>
		<th>Id</th>
		<th>Title</th>
        <th>Created</th>
        <th>Delete</th>
        <th>Edit</th>
        
	</tr>
	<?php foreach ($articles as $article): ?>
    <tr>
        <td><?= $article->id ?></td>
        <td>
            <?= $this->Html->link($article->title, ['action' => 'view', $article->id]) ?>
        </td>
        <td>
            <?= $article->created->format(DATE_RFC850) ?>
        </td>
        <td>
        	<?= $this->Form->postLink('Delete',['action' => 'delete',$article->id],['confirm'=>"Are you sure?"]); 
        	?>
        </td>
        <td>
        	<?= $this->Html->link('Edit',['action'=>'edit',$article->id])?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>