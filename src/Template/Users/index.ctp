<h1>Blog Users</h1>
<table>
	<tr>
		<th>Id</th>
		<th>Username</th>
		<th>Password</th>
		<th>Role</th>
		<th>Created</th>
		<th>Modified</th>
	</tr>
	<?php foreach($users as $user):?>
	<tr>
		<td> <?= $user->id?></td>
		<td><?= $this->Html->link($user->username,['action'=>'view',$user->id])?></td>
		<td><?= $this->Html->link($user->password,['action'=>'view',$user->password])?></td>
		<td><?= $this->Html->link($user->role,['action'=>'view',$user->role])?></td>
		<td><?= $this->Html->link($user->created,['action'=>'view',$user->created])?></td>
		<td><?= $this->Html->link($user->modified,['action'=>'view',$user->modified])?></td>
		

	</tr>
	<?php endforeach; ?>
</table>