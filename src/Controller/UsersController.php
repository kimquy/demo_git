<?php 
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Validation\Validator;

class UsersController extends AppController{
	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
		$this->Auth->allow(['add','logout']);
	}

	public function index(){
		$this->set('users',$this->Users->find('all'));
		if($this->Auth->user()['role']=='admin'){
			$users = $this->Users->find('all');
			$this->set(compact('user'));
		}
		else{
			$this->Flash->error(__('Can not access page!'));
			  return $this->redirect(['controller' => 'Users', 'action' => 'login']);
		}
	}
	public function view($id){
		$user = $this->Users->get($id);
		$this->set(compact('user'));
	}
	public function add(){
		$user = $this->Users->newEntity();
		if($this->request->is('post')){
			$user = $this->Users->patchEntity($user,$this->request->getData());
			if($this->Users->save($user)){
				$this->Flash->success(__('The user has beeb saved'));
				return $this->redirect(['action'=>'add']);
			}
			$this->Flash->error(__('Unable to add the user'));

		}
		$this->set('user',$user);
	}
	public function login()
    {
    	//$validator = $this->Users->validatorLogin();
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
}
?>